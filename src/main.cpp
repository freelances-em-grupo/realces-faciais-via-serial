#include <Arduino.h>
// é muito bacana usar Vim!
//define output pin
#define START 8
#define STOP 9
#define UP 10
#define DOWN 11
#define RIGHT 12
#define LEFT 13

//define byte serial comunication
#define HSTART 0x71
#define HSTOP 0x65
#define HUP 0x77
#define HDOWN 0x73
#define HRIGHT 0x64
#define HLEFT 0x61

void turnOff()
{
    digitalWrite(START,0);
    digitalWrite(STOP,0);
    digitalWrite(UP,0);
    digitalWrite(DOWN,0);
    digitalWrite(RIGHT,0);
    digitalWrite(LEFT,0);
}

void serialEvent()
{
    while (Serial.available())
    {
        byte baite = Serial.read();
        switch (baite)
        {
        //Start
        case HSTART: //q
            //Desativa STOP
            digitalWrite(STOP,0);
            //Ativa START
            digitalWrite(START,1);
            break;

        //Stop
        case HSTOP: //e
            //Desativa START
            digitalWrite(START,0);
            //Ativa STOP
            digitalWrite(STOP,1);
            break;
        
            //Up
        case HUP: //w
            //Desativa DOWN
            digitalWrite(DOWN,0);
            //Ativa OUTPUT
            digitalWrite(UP,1);
            break;

        //DOWN
        case HDOWN: //s
            //Desativa UP
            digitalWrite(UP,0);
            //Ativa DOWN
            digitalWrite(DOWN,1);
            break;

        //Right
        case HRIGHT: //d
            //Desativa LEFT
            digitalWrite(LEFT,0);
            //Ativa RIGHT
            digitalWrite(RIGHT,1);
            break;
            
        //Left
        case HLEFT: //a
            //Desativa RIGHT
            digitalWrite(RIGHT,0);
            //Ativa LEFT
            digitalWrite(LEFT,1);
            break;

        default:
            Serial.println("unknown serial function");
        }
    }
}

void setup()
{
    Serial.begin(9600);

    pinMode(START, OUTPUT);
    pinMode(STOP, OUTPUT);
    pinMode(UP, OUTPUT);
    pinMode(DOWN, OUTPUT);
    pinMode(RIGHT, OUTPUT);
    pinMode(LEFT, OUTPUT);

    digitalWrite(START, 0);
    digitalWrite(STOP, 0);
    digitalWrite(UP, 0);
    digitalWrite(DOWN, 0);
    digitalWrite(RIGHT, 0);
    digitalWrite(LEFT, 0);
}

void loop()
{
}
