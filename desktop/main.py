# coding:utf-8


#imports para a interface (Kivy):
from kivy.app import App
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.graphics.texture import Texture

#imports para o tratamento de imagem:
import cv2

rostos_classificador = cv2.CascadeClassifier('data/olhos.xml')


class KivyCamera(Image):
    def __init__(self, capture, fps, **kwargs):
        super(KivyCamera, self).__init__(**kwargs)
        self.capture = capture
        #o schedule é um timer do python a cada (1.0/fps)s ele chama o self.update
        Clock.schedule_interval(self.update, 1.0 / fps)

    #atualiza o frame na camera, por hora também desenha o quadrado sobre o rosto
    def update(self, dt):
        ret, frame = self.capture.read()
        if ret:
            cinza = cv2.cvtColor(cv2.flip(frame, 0), cv2.COLOR_BGR2GRAY)
            colorida = cv2.cvtColor(cinza, cv2.COLOR_GRAY2BGR)
            #só coloque uma linha dessas se não o processo fica bem lento hehe
            rostos = rostos_classificador.detectMultiScale(cinza)
            for r in rostos:
                x0,y0,w,h = r
                x1 = x0 + w
                y1 = y0 + h
                r = int(255)
                g = int(0)
                b = int(0)
                cv2.rectangle(colorida, (x0,y0), (x1,y1), (b,g,r), 2)
            
            # converte a imagem em textura compátivel com a do Kivy
            buf1 = colorida
            buf = buf1.tostring()
            image_texture = Texture.create(
                size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
            image_texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            # mostra via kivy a textura
            self.texture = image_texture


class CamApp(App):
    def build(self):
        #o numero indica a camera, normalmente 0 indica a camera local (notebook)
        self.capture = cv2.VideoCapture(0)
        
        self.title = "Reconhecedor de expressões"
        #self.icon = 'icone-maneiro.png'

        #fps 30 é mais que o sufiiente, até para não forçar o processamento
        self.my_camera = KivyCamera(capture=self.capture, fps = 30)

        #Layout:
        label1 = Label(text="", pos_hint = {'x': .45, 'center_y': .98}, size_hint=(None, None))
        buttonL =  Button(text = 'Start', size_hint = (0.5,0.1))
        buttonD =  Button(text = 'Stop', size_hint = (0.5,0.1))

        buttonw =  Button(text = 'Frente', size_hint = (0.3,0.1))
        buttona =  Button(text = 'Esquerda', size_hint = (0.3,0.1))
        buttond =  Button(text = 'Direita', size_hint = (0.3,0.1))
        buttons =  Button(text = 'Ré', size_hint = (0.3,0.1))

        layoutVideo = FloatLayout()
        layoutBotoes = BoxLayout(orientation = 'horizontal', size_hint = (0.8,0.8), pos_hint={'x': .1, 'center_y': .45},)
        layoutJoystick = GridLayout(cols = 3, size_hint = (0.5,0.2))
        
        layoutJoystick.add_widget(Label(text = "", size_hint = (0.3,0.1)))
        layoutJoystick.add_widget(buttonw)
        layoutJoystick.add_widget(Label(text = "", size_hint = (0.3,0.1)))
        layoutJoystick.add_widget(buttona)
        layoutJoystick.add_widget(buttons)
        layoutJoystick.add_widget(buttond)

        layoutBotoes.add_widget(buttonL)
        layoutBotoes.add_widget(buttonD)
        layoutBotoes.add_widget(Label(text = "", size_hint = (0.3,0.1)))
        layoutBotoes.add_widget(layoutJoystick)

        layoutVideo.add_widget(label1)
        layoutVideo.add_widget(self.my_camera)
        layoutVideo.add_widget(layoutBotoes)

        return layoutVideo

    #sem isso o app não desliga a camera
    def on_stop(self):
        self.capture.release()


if __name__ == '__main__':
    CamApp().run()