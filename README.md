## Resumo

    Este projeto tem o intuito de via reconhecimento de expressões faciais, controlar pinos de um Arduino Uno
Para que de acorco com a expressão do usuário o mesmo possa controlar uma cadeira de rodas, com os comandos:

* Start
* Stop
* Frente
* Ré
* Direita
* Esquerda


## Detalhes do código

    O código para o desktop foi implementado com Python, utilizando as bibliotecas OpenCV e Kivy, para o reconhecimento
das expressões e para a interface gráfica respectivamente, além da Pyserial para a comunicação entre o Arduino e o computador,
talvez seja utilizado o PlatformIo diretamente para isto.

    Por sua vez o código do Arduino foi feito utilizando o sitema PlatformIO, que dá maiores possibilidades e profissionalismo para a
programação do Arduino ao invez de se utilizar a IDE do Arduino com seu framework, o código é em C++, com o framework Arduino
servindo de interface entre o código fonte e o hardware do Atmega.


## Instalação

   Para o desktop é necessário utilizar Python 2.7 e via ``pip install -r requeriments.txt`` instalar as bibliotecas necessárias

   Para o Arduiino Uno é necessário abrir o fonte diretamente no PlatFormIO e enviar para o mesmo.


## Contribuidores

|          **Nome**         |  **Contato do Telegram** |
|:---------------------:|:--------------------:|
|    Michel Martinez    |   @MichelMartinez    
|      Pablo Diehl      |      @pablodiehl     
|  Roberto Biegelmeyer  |     @robertobiegel   


## License

   Este código segue as licenças de suas bibliotecas utilizadas, por tanto é distribuito livremente para quem queira para fins educacionais ou comerciais. Licença MIT.
